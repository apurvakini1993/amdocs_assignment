<?php

include_once 'dbConfig.php';

function getFilteredData($postFrom,$postTo) {
    $con = getdb();

    $dateFrom = !empty($postFrom) ? date('m/d/Y', strtotime($postFrom)) : "";
    $dateTo = !empty($postTo) ? date('m/d/Y', strtotime($postTo)) : "";

    if (!empty($postFrom) && !empty($postTo)) {
        $sql = "
  SELECT * FROM exceldata 
  WHERE start_date >= '" . $dateFrom . "' AND end_date <= '" . $dateTo . "'";
    } else if (!empty($postFrom) && empty($postTo)) {
        $sql = "
  SELECT * FROM exceldata 
  WHERE start_date = '" . $dateFrom . "'";
    } else if (empty($postFrom) && !empty($postTo)) {
        $sql = "
  SELECT * FROM exceldata 
  WHERE end_date = '" . $dateTo . "'";
    } else {
        $sql = "SELECT * FROM exceldata";
    }

    $result = mysqli_query($con, $sql);

    return $result;
}

?>