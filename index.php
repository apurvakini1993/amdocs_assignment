<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
    <body>
        <div id="wrap">
            <div class="container">
                <div class="row">
                    <br>
                    <br>
                    <h3 class="txt-center">Amdocs Assignment</h3>
                    <br>
                    <div class="col-sm-4 col-md-offset-4 ">

                        <form class="form-horizontal" action="uploadExcel.php" method="post" name="upload_excel" enctype="multipart/form-data">
                            <fieldset>
                                <!-- File Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="filebutton">Select File</label>
                                    <div class="col-md-4">
                                        <input type="file" name="file" id="file" class="input-large" required>
                                    </div>
                                </div>
                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                                    <div class="col-md-4">
                                        <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>


                </div>
                
                <br>

                <div class="row">
                    <div class="col-sm-8 col-md-offset-3"> 
                        <form class="form-group" method='post' style="margin-bottom: -5px;">
                            Start Date:&nbsp; <input class="form-group" type='date' class='dateFilter' name='dateFrom' value='<?php if (isset($_POST['dateFrom'])) echo $_POST['dateFrom']; ?>'>

                            End Date:&nbsp; <input class="form-group" type='date' class='dateFilter' name='dateTo' value='<?php if (isset($_POST['dateTo'])) echo $_POST['dateTo']; ?>'>

                            <input type='submit' class="btn btn-default" name='search' value='Search'>
                        </form> 
                    </div>
                </div>

                <?php
                require 'dbConfig.php';
                require 'showSessionMessage.php';
                require 'tableRecords.php';
                getAllRecords();
                ?>
            </div>
        </div>
    </body>

</html>


