<?php
include_once 'dbConfig.php';

$id = $_REQUEST['id'];
$query = "SELECT * from exceldata where id='" . $id . "'";
$result = mysqli_query(getdb(), $query) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Update Record</title>

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
    </head>
    <body>
        <br><br>
        <div class="form col-md-6 col-md-offset-3">
            <h3 class="txt-center">Update Data</h3>
            <?php require 'showSessionMessage.php'; ?>
            <div>
                <form name="form" method="post" action="dbConfig.php"> 
			<input name="id" type="hidden" value="<?php echo $row['id']; ?>" />
                    <div class="form-group">
                        <label for="activity">Activity:</label>
                        <input  class="form-control" type="text" name="activity" placeholder="Enter Activity" 
                                required value="<?php echo $row['activity']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="start_date">Start Date:</label>
                        <input class="form-control" type="date" name="start_date" placeholder="Enter Start Date" 
                               required value="<?php echo strftime('%Y-%m-%d',
        strtotime($row['start_date']));
?>" />
                    </div>
                    <div>
                        <label for="end_date">End Date:</label>
                        <input class="form-control" type="date" name="end_date" placeholder="Enter End Date" 
                               required value="<?php echo strftime('%Y-%m-%d',
                                       strtotime($row['end_date']));
?>" />
                    </div>  
                        
                    <div>
                        <label for="owner">Owner:</label>
                        <input class="form-control" type="text" name="owner" placeholder="Enter Owner" 
                               required value="<?php echo $row['owner']; ?>" />
                    </div>    

                    <div>
                        <label for="port">Port:</label>
                        <input class="form-control" type="number" name="port" placeholder="Enter Port" 
                               required value="<?php echo $row['port']; ?>" />
                    </div>  
                    <div>
                        <label for="status">Status:</label>
                        <input class="form-control" type="text" name="status" placeholder="Enter Status" 
                               required value="<?php echo $row['status']; ?>" />
                    </div>  
                    <br>
                    <div class="txt-center">
                    <input name="update" type="submit" value="Update" class="btn btn-default" />
                    </div>
                </form>

            </div>
        </div>
    </body>
</html>