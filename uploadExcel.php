<?php

require 'vendor/autoload.php';

require 'dbConfig.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;

if (isset($_POST["Import"])) {
    if ($_FILES["file"]["size"] > 0) {

        $fname = $_FILES["file"]["name"];

        $ext = pathinfo($fname, PATHINFO_EXTENSION);
        if ($ext !== "xlsx") {
            echo "<script type=\"text/javascript\">
            alert(\"Invalid File Extension\");
            window.location = \"index.php\"
	    </script>";
            return;
        }


        $filename = $_FILES["file"]["tmp_name"];

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $spreadsheet = $reader->load($filename);

        $spreadsheet->getActiveSheet()->getStyle('C')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_MMDDYYYY);


        $spreadsheet->getActiveSheet()->getStyle('D')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_MMDDYYYY);



        $sheetData = $spreadsheet->getActiveSheet()->toArray();

        // print_r($sheetData);exit;
        $i = 1;

        unset($sheetData[0]);

        foreach ($sheetData as $t) {
            $test_arr = explode('/', $t[2]);
            $errors = "";
            if (empty($t[1]) || !is_numeric($t[5]) || !checkdate($test_arr[0], $test_arr[1], $test_arr[2]) ) {
       //          echo "<script type=\"text/javascript\">
							// alert(\"Invalid File:Activity column data cannot be empty.\");
							// window.location = \"index.php\"
						 //  </script>";
                $errors.= "Invalid File:Activity column data cannot be empty.<br>
                Invalid File:Port column data has to be numeric..<br>
                Invalid Start Date<br>
                " ;         
                
            }

       //      else if (!is_numeric($t[5])) {

       // //          echo "<script type=\"text/javascript\">
							// // alert(\"Invalid File:Port column data has to be numeric.\");
							// // window.location = \"index.php\"
						 // //  </script>";

       //              $errors.= "Invalid File:Port column data has to be numeric..<br>" ;       

               
       //      }   
       //      else if (!checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
       // //          echo "<script type=\"text/javascript\">
							// // alert(\"Invalid Start Date\");
							// // window.location = \"index.php\"
						 // //  </script>";
       //                   $errors.= "Invalid Start Date<br>" ;  
                
       //      }

            //echo $errors;

             echo "<script type=\"text/javascript\">
            alert(".$errors.");
            window.location = \"index.php\"
        </script>";

        return;

    //      $_SESSION['message'] = "<span style='color:green'>".$errors."</span>";

    // header('location: index.php');

            
            
        }


        foreach ($sheetData as $t) {


// Creating new date format from that timestamp


            $sql = "INSERT into exceldata (activity,start_date,end_date,owner,port,status) 
                
                   values ('" . $t[1] . "',
                        '" . $t[2] . "' ,
                    '" . $t[3] . "',
                    '" . $t[4] . "',
                    '" . $t[5] . "',
                    '" . $t[6] . "'
                       
                    )";

            $conn = getdb();


            $result = mysqli_query($conn, $sql);

            if (!isset($result)) {
                echo "<script type=\"text/javascript\">
							alert(\"Invalid File:Please Upload CSV File.\");
							window.location = \"index.php\"
						  </script>";
                return;
            } else {
                echo "<script type=\"text/javascript\">
						alert(\"CSV File has been successfully Imported.\");
						window.location = \"index.php\"
					</script>";
            }


            $i++;
        }
    }
}
?>