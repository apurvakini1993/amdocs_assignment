<?php
include "getFilteredData.php";

function getAllRecords() {
    
    $postFrom = isset($_POST['dateFrom']) ? $_POST['dateFrom'] :"";
    $postTo = isset($_POST['dateTo']) ? $_POST['dateTo'] :"";
    
   //print_r($resultData);exit;
   $result = getFilteredData($postFrom,$postTo);
    

    if (mysqli_num_rows($result) > 0) {
        echo "
        <div class='table-responsive'>
        <form action='exportSheet.php' method='post'>
        <input type='hidden' name='start_date' value='$postFrom'/>
        <input type='hidden' name='end_date' value='$postTo'/>    
        <button type='submit' class='btn btn-info' style='float:right;margin-bottom:10px;'> Export Data</button>    
        </form>
        
        <table id='myTable' class='table table-striped table-bordered'>
             <thead><tr><th> ID</th>
                          <th>Activity</th>
                          <th>Start Date</th>
                          <th>End Date</th>
                          <th>Owner</th>
                          <th>Port</th>
                          <th>Status</th>
                          <th>Edit</th>
                        </tr></thead><tbody>";
        while ($row = mysqli_fetch_assoc($result)) {

            echo "<tr><td>" . $row['id'] . "</td>
                   <td>" . $row['activity'] . "</td>
                   <td>" . $row['start_date'] . "</td>
                   <td>" . $row['end_date'] . "</td>
                   <td>" . $row['owner'] . "</td>
                   <td>" . $row['port'] . "</td>
                   <td>" . $row['status'] . "</td>
                   <td><a href=editSheet.php?id=" . $row['id'] . " style='text-align:center;text-decoration: none !important;'><span class='btn-success form-control'>Edit</span></td>
                   </tr>";
        }

        echo "</tbody></table></div>";
    } else {
        echo "<br><h5 class='txt-center'>you have no records</h5>";
    }
}


?>