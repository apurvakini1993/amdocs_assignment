<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require 'dbConfig.php';

include "getFilteredData.php";


// Creates New Spreadsheet 
$spreadsheet = new Spreadsheet();

// Retrieve the current active worksheet 
$sheet = $spreadsheet->getActiveSheet();

//$data_from_db = array();
//$con = getdb();
//$Sql = "SELECT * FROM exceldata";
$res = getFilteredData($_POST['start_date'],$_POST['end_date']);


$column_header = ["Sr.No", "Activity", "Start Date", "End Date", "Owner", "Port", "Status"];
$j = 1;
foreach ($column_header as $x_value) {
    $sheet->setCellValueByColumnAndRow($j, 1, $x_value);
    $sheet->getStyle(1)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('8EA9DB');
    $sheet->getStyle(1)->getFont()->setBold( true );
    $j = $j + 1;
}

$rowCount = 2;
while ($row =  mysqli_fetch_assoc($res)) {

    $sheet->setCellValue('A' . $rowCount, $row['id']);
    $sheet->setCellValue('B' . $rowCount, $row['activity']);
    $sheet->setCellValue('C' . $rowCount, $row['start_date']);
    $sheet->setCellValue('D' . $rowCount, $row['end_date']);
    $sheet->setCellValue('E' . $rowCount, $row['owner']);
    $sheet->setCellValue('F' . $rowCount, $row['port']);
    $sheet->setCellValue('G' . $rowCount, $row['status']);

    $rowCount++;
}

// Write an .xlsx file  
$writer = new Xlsx($spreadsheet);

// Save .xlsx file to the files directory 
$filename = uniqid(rand(), true) . '.xlsx';
fopen($filename, 'wb');

$writer->save('assets/' . $filename);

$filepath = 'assets/' . $filename;

if (file_exists($filepath)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filepath));
    flush(); // Flush system output buffer
    readfile($filepath);
    die();
} else {
    http_response_code(404);
    die();
}
?>