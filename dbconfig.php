<?php

function getdb() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $db = "amdocs_assignment";

    try {

        $conn = mysqli_connect($servername, $username, $password, $db);
    } catch (exception $e) {
        echo "Connection failed: " . $e->getMessage();
    }
    return $conn;
}

    session_start();
    $db = getdb();
    $activity = "";
    $start_date = "";
    $end_date = "";
    $port = "";
    $status = "";
    $update = false;

if (isset($_POST['update'])) {

   // echo "hi";exit;
    $activity = $_REQUEST['activity'];
    //echo $activity;exit;

    $start_date_format = new DateTime($_REQUEST['start_date']);
    $start_date = $start_date_format->format('m/d/Y');
    $end_date_format = new DateTime($_REQUEST['end_date']);
    $end_date = $end_date_format->format('m/d/Y');
    $owner = $_REQUEST['owner'];
    $port = $_REQUEST['port'];
    $status = $_REQUEST['status'];
    $id = $_REQUEST['id'];
    
    if (strtotime($end_date ) <= strtotime( $start_date ) ||
    strtotime( $start_date ) >= strtotime( $end_date ) ) {
      $_SESSION['message'] = "<span style='color:Red'>End date cannot be Smaller than Start Date</span>";
    
    header('location: index.php');
    } else {
         $update = "update exceldata set activity='" . $activity . "',
                start_date='" . $start_date . "',
                end_date='" . $end_date . "',
                owner='" . $owner . "',
                port='" . $port . "',
                status='" . $status . "'
                where id='" . $id . "'";
    
  
    mysqli_query($db, $update) or die(mysqli_error());

    $_SESSION['message'] = "<span style='color:green'>Record Updated Successfully</span>";

    header('location: index.php');
    }


   
}
?>